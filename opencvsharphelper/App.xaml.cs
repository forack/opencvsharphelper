﻿using ModuleCore.Common;
using ModuleCore.Mvvm;

using OpencvsharpModule.Models;
using OpencvsharpModule.Views;

using Prism.Ioc;
using Prism.Modularity;

using System.Linq;
using System.Reflection;
using System.Windows;

namespace opencvsharphelper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App
    {
        protected override Window CreateShell()
        {
            //设置程序的运行时环境
            System.Environment.SetEnvironmentVariable("Path", @"D:/OpenCV/Runtimes/opencv452/;D:/OpenCV/Runtimes/paddleocr/;D:/OpenCV/Runtimes/zbarX64Runtime/;D:/OpenCV/Runtimes/BaslerRuntimeX64/;D:/OpenCV/Runtimes/MVSRuntimeX64/;C:\Program Files\Basler\pylon 6\Runtime\x64;C:\Program Files (x86)\Common Files\MVS\Runtime\Win64_x64;");
            var Navigate = Container.Resolve<NavigateModel>();
            var views = from t in Assembly.GetExecutingAssembly().GetTypes()   //
                        where t.GetCustomAttribute<NavigationPageAttribute>() is not null
                        select t;
            foreach (var view in views)
            {
                var page = view.GetCustomAttribute<NavigationPageAttribute>();
                Navigate.NavigateList.Add(new NavigateItem()
                {
                    ViewName = page.ViewName,
                    IconKind = page.IconKind,
                    DisplayName = page.DisplayName,
                    UserLevel = page.UserLevel,
                    Display = page.Display
                });
            }
            //设置默认页面
            Navigate.DefaultView = "CameraView";


            //启动Core中的主窗体
            return null;

        }

        protected override void RegisterTypes(IContainerRegistry containerRegistry)
        {
            _ = containerRegistry.RegisterSingleton<NavigateModel>();
            containerRegistry.RegisterSingleton<CalibrateCommon>();
            containerRegistry.RegisterSingleton<CameraModel>();
            containerRegistry.RegisterSingleton<ImagePool>();
            containerRegistry.RegisterSingleton<DataPool>();
            containerRegistry.RegisterSingleton<RoslynEditorModel>();

            containerRegistry.RegisterDialog<CalibrateView, OpencvsharpModule.ViewModels.CalibrateViewModel>();

            containerRegistry.RegisterForNavigation<CameraView>();
            containerRegistry.RegisterForNavigation<ThresholdView>();
            containerRegistry.RegisterForNavigation<MorphologyView>();
            containerRegistry.RegisterForNavigation<HoughLinesView>();
            containerRegistry.RegisterForNavigation<RoslynView>();
            containerRegistry.RegisterForNavigation<FeatureMatchingView>();
            containerRegistry.RegisterForNavigation<ConnectedView>();
            containerRegistry.RegisterForNavigation<MaskCopyView>();
            containerRegistry.RegisterForNavigation<CannyView>();
            containerRegistry.RegisterForNavigation<HogSvmView>();
            containerRegistry.RegisterForNavigation<CornersView>();
        }

        protected override void ConfigureModuleCatalog(IModuleCatalog moduleCatalog)
        {
            _ = moduleCatalog.AddModule<ModuleCore.CoreModule>();


        }
    }
}